JSONPath
========

A query helper language to be used within JSON objects. This library need to provide the same functionalities as XPath for XML.